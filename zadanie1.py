import datetime, os, sys

class MinDateFile:

    MIN_DATE = None

    def __init__(self, file_path, *args, **kwargs):
        self.file_path = file_path
    
    def load_file(self):
        content = None
        with open(os.path.join(os.getcwd(), self.file_path), 'r') as _file:
            content = _file.readlines()
        return content

    def parse_content(self, file_content):
        for idx, item in enumerate(file_content):
            single_line_parsed = self.parse_single_line(item)
            if not single_line_parsed:
                continue
            self.create_date_object([self.format_year_param(single_line_parsed[0]), *single_line_parsed[1:]])    
    
    def format_year_param(self, year):
        if len(str(year)) == 4:
            return year
        out = [2]
        for i in range(4-len(str(year))-1):
            out.append(0)
        out.append(year)
        return int("".join([str(item) for item in out]))

    def create_date_object(self, params):
        try:
            date_object = datetime.date(*params)
            self.set_min_date(date_object)
            return date_object
        except ValueError:
            raise ValueError('is_illegal')

    def set_min_date(self, date):
        if not self.MIN_DATE or date < self.MIN_DATE:
            self.MIN_DATE = date

    def parse_single_line(self, single_line):
        try:
            single_line = sorted([int(item) for item in single_line.split('/')])
            return self.parse_full_year_given(single_line)
        except ValueError:
            # let the program to check other lines of file
            pass

    def parse_full_year_given(self, single_line):
        full_year = [(idx, item) for idx, item in enumerate(single_line) if len(str(item)) == 4]
        if full_year:
            year = single_line.pop(full_year[0][0])
            return [year, *single_line]
        return single_line


    def execute(self):
        self.parse_content(self.load_file())
        return self.MIN_DATE.strftime('%Y-%m-%d')


if __name__ == '__main__':
    print(MinDateFile(sys.argv[1]).execute())